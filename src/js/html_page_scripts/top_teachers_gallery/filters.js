import { getTopTeachers } from '../../users_objects_processing/processing/users_objects_processing';
import { SRC_FIELD } from '../teacher_compact_card';
import showTeachers from './display';

let topTeachers = [];
let teachersDisplayed = topTeachers;

function getShowWithPhotoCheckbox() {
  return document.getElementById('show-with-photo-checkbox');
}

function getShowFavouritesCheckbox() {
  return document.getElementById('show-favourites-checkbox');
}

function getTeachersWithPhoto(teachers) {
  return teachers.filter((teacher) => teacher[SRC_FIELD]);
}

function getFavouriteTeachers(teachers) {
  return teachers.filter((teacher) => teacher.favorite === true);
}

function showTeachersWithPhoto(event) {
  const showWithPhoto = event.currentTarget.checked;
  if (showWithPhoto) {
    teachersDisplayed = getTeachersWithPhoto(teachersDisplayed);
  } else {
    const showFavourite = getShowFavouritesCheckbox().checked;
    if (showFavourite) {
      teachersDisplayed = getFavouriteTeachers(topTeachers);
    } else {
      teachersDisplayed = topTeachers;
    }
  }
  showTeachers(teachersDisplayed);
}

function showFavouriteTeachers(event) {
  const showFavourite = event.currentTarget.checked;
  if (showFavourite) {
    teachersDisplayed = getFavouriteTeachers(teachersDisplayed);
  } else {
    const showWithPhoto = getShowWithPhotoCheckbox().checked;
    if (showWithPhoto) {
      teachersDisplayed = getTeachersWithPhoto(topTeachers);
    } else {
      teachersDisplayed = topTeachers;
    }
  }
  showTeachers(teachersDisplayed);
}

export function addListenersToTopTeachersGallery() {
  const showWithPhotoCheckbox = getShowWithPhotoCheckbox();
  showWithPhotoCheckbox.addEventListener('click', showTeachersWithPhoto);
  const showFavouritesCheckbox = getShowFavouritesCheckbox();
  showFavouritesCheckbox.addEventListener('click', showFavouriteTeachers);
}

export function updateTopTeachers() {
  topTeachers = getTopTeachers();
  teachersDisplayed = topTeachers;
}
