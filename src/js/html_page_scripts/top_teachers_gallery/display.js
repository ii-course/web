import { getTopTeachers } from '../../users_objects_processing/processing/users_objects_processing';
import { getTeachersCards } from '../teacher_compact_card';

function getGalleryElement() {
  return document.getElementById('top-teachers-table-gallery');
}

function getFragmentForGallery(teachers) {
  const teachersCards = getTeachersCards(teachers);
  const fragment = document.createDocumentFragment();
  teachersCards.forEach((card) => fragment.appendChild(card));
  return fragment;
}

function clearGallery() {
  const gallery = getGalleryElement();
  gallery.innerHTML = '';
}

function fillGallery(teachers) {
  const gallery = getGalleryElement();
  const fragment = getFragmentForGallery(teachers);
  gallery.appendChild(fragment);
}

export default function showTeachers(teachers = getTopTeachers()) {
  clearGallery();
  fillGallery(teachers);
}
