import { handleStarIcon } from '../../teacher_compact_card';
import { hidePopup } from '../display';
import {
  FILLED_STAR, getPopupStarButton, getPopupStarIcon, changeStarIconFill,
} from './star_button';
import { markUserByIdAsFavourite } from '../../../users_objects_processing/users_for_ui_with_changes_tracked';
import { getUserIdFromElement } from '../../html_elements_processing';
import showFavouriteTeachers from '../../line_gallery/display';
import { addListenerToToggleButton } from './map/toggle_listener';

function getCompactCardsByTeacherId(id) {
  const compactCards = [...document.getElementsByClassName('teacher-card-compact')];
  return compactCards.filter((card) => card.dataset.id === id);
}

function getTeacherId() {
  const popup = document.getElementById('popup-teacher-info');
  return getUserIdFromElement(popup);
}

function hasTeacherBecomeFavourite() {
  const star = getPopupStarIcon();
  return star.className === FILLED_STAR;
}

function markTeacherAsFavouriteIfNeeded() {
  const hasBecomeFavourite = hasTeacherBecomeFavourite();
  const teacherId = getTeacherId();
  markUserByIdAsFavourite(hasBecomeFavourite, teacherId);
}

function changeStarFillInTeacherCompactCards() {
  const hasBecomeFavourite = hasTeacherBecomeFavourite();
  const teacherId = getTeacherId();
  const compactCardsForTeacher = getCompactCardsByTeacherId(teacherId);
  compactCardsForTeacher.forEach((card) => handleStarIcon(card, hasBecomeFavourite));
}

function hideTeacherInfoPopup() {
  hidePopup('wrapper-teacher-info');
}

function addListenersToCloseButton() {
  const closeButton = document.getElementById('popup-teacher-info-close-button');
  closeButton.addEventListener('click', hideTeacherInfoPopup);
  closeButton.addEventListener('click', changeStarFillInTeacherCompactCards);
  closeButton.addEventListener('click', markTeacherAsFavouriteIfNeeded);
  closeButton.addEventListener('click', showFavouriteTeachers);
}

function addListenersToStarButton() {
  const starButton = getPopupStarButton();
  starButton.addEventListener('click', changeStarIconFill);
}

export default function addListenersToTeacherInfoPopup() {
  addListenersToCloseButton();
  addListenersToStarButton();
  addListenerToToggleButton();
}
