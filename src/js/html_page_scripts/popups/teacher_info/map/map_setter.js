import L from 'leaflet';

let map;
let marker;

function setMarker(center) {
  if (marker) {
    marker.remove();
  }
  marker = L.marker(center);
  marker.addTo(map);
}

function initializeMap() {
  const mapOptions = {
    center: [0.0, 0.0],
    zoom: 10,
  };
  // eslint-disable-next-line new-cap
  map = new L.map('map', mapOptions);
  const layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  map.addLayer(layer);
}

export default function setMap(coordinates) {
  if (coordinates && coordinates.latitude && coordinates.longitude) {
    if (!map) {
      initializeMap();
    }
    const center = [coordinates.latitude, coordinates.longitude];
    console.log(center);
    const zoom = 10;
    map.setView(center, zoom);
    setMarker(center);
  }
}
