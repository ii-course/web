export function getMapContainer() {
  return document.getElementById('map');
}

export function getToggleButton() {
  return document.getElementById('toggle-map-button');
}

export function getGeneralInfoContainer() {
  return document.getElementById('teacher-general-info-container');
}
