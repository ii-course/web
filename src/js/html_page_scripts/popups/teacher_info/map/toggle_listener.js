import { getMapContainer, getToggleButton, getGeneralInfoContainer } from './elements_getter';

function setMapDisplayed(isDisplayed) {
  const mapContainer = getMapContainer();
  mapContainer.dataset.mapDisplayed = isDisplayed.toString();
}

export function isMapDisplayed() {
  const mapContainer = getMapContainer();
  return mapContainer.dataset.mapDisplayed === 'true';
}

function hideInfoContainer() {
  const infoContainer = getGeneralInfoContainer();
  infoContainer.style.display = 'none';
}

function displayInfoContainer() {
  const infoContainer = getGeneralInfoContainer();
  infoContainer.style.display = 'block';
}

function hideMap() {
  const mapContainer = getMapContainer();
  mapContainer.style.display = 'none';
  setMapDisplayed(false);
}

function displayMap() {
  const mapContainer = getMapContainer();
  mapContainer.style.display = 'block';
  setMapDisplayed(true);
}

function changeTextOnToggleButton() {
  const toggleButton = getToggleButton();
  toggleButton.textContent = toggleButton.textContent === 'toggle info' ? 'toggle map' : 'toggle info';
}

export function addListenerToToggleButton() {
  const toggleButton = getToggleButton();
  toggleButton.addEventListener('click', () => {
    const mapDisplayed = isMapDisplayed();
    if (mapDisplayed) {
      hideMap();
      displayInfoContainer();
    } else {
      hideInfoContainer();
      displayMap();
    }
    changeTextOnToggleButton();
  });
}
