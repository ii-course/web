import dayjs from 'dayjs';
import { users } from '../../../users_objects_processing/users_for_ui_with_changes_tracked';
import { showPopup } from '../display';
import { findById } from '../../../users_objects_processing/processing/search';
import { fillStarIcon } from './star_button';
import { getUserIdFromElement } from '../../html_elements_processing';
import { isArabic } from '../../../strings_processing';
import setMap from './map/map_setter';
import { isMapDisplayed } from './map/toggle_listener';
import { getToggleButton } from './map/elements_getter';

const SRC_FIELD = 'picture_large';

function getIdForField(field) {
  return `popup-teacher-info-${field}`;
}

function addUserIdToPopup(id) {
  const popup = document.getElementById('popup-teacher-info');
  popup.dataset.id = id;
}

function clearPopupContent() {
  const img = document.getElementById('popup-teacher-info-img');
  img.setAttribute('src', '');
  const fieldsToClearTextContent = ['name', 'city', 'country', 'age', 'gender', 'email', 'phone'];
  fieldsToClearTextContent.forEach((field) => {
    const id = getIdForField(field);
    document.getElementById(id).textContent = '';
  });
}

function fillPopupFieldsWithValues(fieldWithValues) {
  Object.keys(fieldWithValues).forEach((field) => {
    const id = getIdForField(field);
    const textToInsert = fieldWithValues[field] ? fieldWithValues[field] : `Unknown ${field}`;
    document.getElementById(id).textContent = textToInsert;
  });
}

function insertImg(src) {
  const img = document.getElementById('popup-teacher-info-img');
  img.setAttribute('src', src);
}

function addImgToPopup(src) {
  let insertBySrc = src;
  const DEFAULT_SRC = './images/user.png';
  if (!src) {
    insertBySrc = DEFAULT_SRC;
  }
  insertImg(insertBySrc);
}

function addUserNameToPopup(name) {
  const id = getIdForField('name');
  const textToInsert = name || 'Unknown name';
  const nameElement = document.getElementById(id);
  if (isArabic(textToInsert)) {
    nameElement.style.textAlign = 'right';
  } else {
    nameElement.style.textAlign = 'left';
  }
  nameElement.textContent = textToInsert;
}

function isInt(n) {
  return n % 1 === 0;
}

function getAbsDifference(date1, date2, unit, float = false) {
  const floatResult = Math.abs(date1.diff(date2, unit, true));
  if (float) {
    return floatResult;
  }
  return Math.ceil(floatResult);
}

function getDaysToBirthday(birthdateRaw) {
  const now = dayjs();
  const birthdate = dayjs(birthdateRaw);
  const difference = getAbsDifference(now, birthdate, 'year', true);
  const nowIsBirthday = isInt(difference);
  if (nowIsBirthday) {
    return 0;
  }
  const yearsToAdd = Math.ceil(difference);
  const nextBirthdate = birthdate.add(yearsToAdd, 'year');
  return getAbsDifference(now, nextBirthdate, 'day');
}

function getFormattedBirthdate(birthdate) {
  return dayjs(birthdate).format('DD.MM.YYYY');
}

function fillPopupWithUserData(user) {
  fillStarIcon(user.favorite);
  addUserIdToPopup(user.id);
  addUserNameToPopup(user.full_name);
  addImgToPopup(user[SRC_FIELD]);
  const fieldsWithValues = {
    city: user.city,
    country: user.country,
    age: user.age,
    gender: user.gender,
    email: user.email,
    phone: user.phone,
    days_to_bday: getDaysToBirthday(user.b_day),
    bday: getFormattedBirthdate(user.b_day),
  };
  fillPopupFieldsWithValues(fieldsWithValues);
  setMap(user.coordinates);
}

function showInfoTab() {
  if (isMapDisplayed()) {
    getToggleButton().click();
  }
}

export default function showTeacherInfoPopup(event) {
  const elementClicked = event.currentTarget;
  const userId = getUserIdFromElement(elementClicked);
  const userFounded = findById(users, userId);
  clearPopupContent();
  fillPopupWithUserData(userFounded);
  showPopup('wrapper-teacher-info');
  showInfoTab();
}
