export const FILLED_STAR = 'fas fa-star';
const UNFILLED_STAR = 'far fa-star';

export function getPopupStarButton() {
  return document.getElementById('popup-teacher-info-star-button');
}

export function getPopupStarIcon() {
  return document.getElementById('popup-teacher-info-star');
}

export function addUserIdToPopup(id) {
  const starIcon = getPopupStarIcon();
  starIcon.dataset.id = id;
}

export function fillStarIcon(makeFilled) {
  const starIcon = getPopupStarIcon();
  if (makeFilled) {
    starIcon.className = FILLED_STAR;
  } else {
    starIcon.className = UNFILLED_STAR;
  }
}

export function changeStarIconFill() {
  const starIcon = getPopupStarIcon();
  const isFilledNow = starIcon.className === FILLED_STAR;
  const makeFilled = !isFilledNow;
  fillStarIcon(makeFilled);
}
