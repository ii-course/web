function getMainContainersOnPage() {
  return [document.getElementsByTagName('header').item(0),
    document.getElementsByTagName('main').item(0),
    document.getElementsByTagName('footer').item(0)];
}

function removeBlurFromEverything() {
  const pageMainContainers = getMainContainersOnPage();
  const BLUR_VALUE = 'none';
  for (let i = 0; i < pageMainContainers.length; i += 1) {
    pageMainContainers[i].style.filter = BLUR_VALUE;
  }
}

function blurEverythingExceptPopup() {
  const pageMainContainers = getMainContainersOnPage();
  const BLUR_VALUE = 'blur(5px)';
  for (let i = 0; i < pageMainContainers.length; i += 1) {
    pageMainContainers[i].style.filter = BLUR_VALUE;
  }
}

function hideWrapperWithPopup(wrapperId) {
  const wrapper = document.getElementById(wrapperId);
  wrapper.style.visibility = 'hidden';
  wrapper.style.opacity = '0';
}

function makeWrapperWithPopupVisible(wrapperId) {
  const wrapper = document.getElementById(wrapperId);
  wrapper.style.visibility = 'visible';
  wrapper.style.opacity = '1';
}

export function hidePopup(wrapperId) {
  removeBlurFromEverything();
  hideWrapperWithPopup(wrapperId);
}

export function showPopup(wrapperId) {
  blurEverythingExceptPopup();
  makeWrapperWithPopupVisible(wrapperId);
}
