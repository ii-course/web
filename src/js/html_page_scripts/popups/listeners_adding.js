import addListenersToTeacherFormPopup from './teacher_form/all_listeners_adding';
import addListenersToTeacherInfoPopup from './teacher_info/listeners';

export default function addListenersToPopups() {
  addListenersToTeacherFormPopup();
  addListenersToTeacherInfoPopup();
}
