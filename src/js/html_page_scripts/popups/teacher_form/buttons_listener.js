import { hideTeacherFormPopup } from './display';

export default function addListenersToCloseButton() {
  const closeButton = document.getElementById('popup-teacher-form-close-button');
  closeButton.addEventListener('click', hideTeacherFormPopup);
}
