import { showPopup, hidePopup } from '../display';

function clearInputs() {
  const inputs = document.querySelectorAll('#add-teacher-form input:not(.gender-checkbox)');
  for (let i = 0; i < inputs.length; i += 1) {
    inputs[i].value = null;
  }
  document.getElementById('male-checkbox').checked = false;
  document.getElementById('female-checkbox').checked = false;
}

export function hideTeacherFormPopup() {
  hidePopup('wrapper-teacher-form');
}

export function showTeacherFormPopup() {
  clearInputs();
  showPopup('wrapper-teacher-form');
}
