import { addUser } from '../../../users_objects_processing/users_for_ui_with_changes_tracked';
import { goToPage } from '../../statistics_table/pagination/pagination';
import { fetchChanges } from '../../current_users_for_statistics/users_update';
import { SERVER_URL } from '../../../users_objects_processing/api/url_constants';
import { validateUser } from '../../../users_objects_processing/validation/validation';

function getInputElement(inputName) {
  return document.getElementById(`${inputName}-teacher-form`);
}

function getGender() {
  return document.querySelector('.gender-checkbox:checked')?.value;
}

function getTeacherFromForm() {
  const inputNames = ['full_name', 'country', 'city', 'phone', 'email', 'background', 'age'];
  const teacher = {};
  inputNames.forEach((inputName) => {
    const inputElement = getInputElement(inputName);
    teacher[inputName] = inputElement?.value;
  });
  teacher.gender = getGender();
  return teacher;
}

function closePopup() {
  const closeButton = document.getElementById('popup-teacher-form-close-button');
  closeButton.click();
}

function addTeacher(event) {
  event.preventDefault();
  const teacher = getTeacherFromForm();
  if (addUser(teacher)) {
    closePopup();
    fetchChanges();
    goToPage(1);
  } else {
    alert('User was not added because of invalid format');
  }
}

function postUser(event) {
  event.preventDefault();
  const teacher = getTeacherFromForm();
  const isValid = validateUser(teacher);
  if (isValid) {
    fetch(SERVER_URL.toString(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(teacher),
    })
      .then((response) => console.log(response.json()))
      .catch((error) => {
        console.error(error);
      });
  } else {
    alert('User was not added because of invalid format');
  }
}

export default function addListenerToSubmit() {
  const form = document.getElementById('add-teacher-form');
  form.addEventListener('submit', addTeacher);
  form.addEventListener('submit', postUser);
}
