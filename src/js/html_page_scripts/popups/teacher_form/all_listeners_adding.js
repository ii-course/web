import addListenersToCloseButton from './buttons_listener';
import addListenerToSubmit from './submit_listener';

export default function addListenersToTeacherFormPopup() {
  addListenersToCloseButton();
  addListenerToSubmit();
}
