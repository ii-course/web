import { sortOrder } from '../../users_objects_processing/processing/sort';

export function getFilterOptions() {
  const form = document.forms.filter;
  const options = {
    ageOperation: form.ageRadio.value,
    country: form.country.value,
    age: form.age.value,
    gender: form.gender.value,
    isFavourite: form.favourite.value,
    getPercentage: form.percentage.checked,
  };
  return options;
}

export function getSortingOptions() {
  const form = document.forms.sort;
  const fieldNamesForInputValues = {
    'full-name': 'full_name',
    birthdate: 'b_day',
    age: 'age',
    country: 'country',
  };
  const fieldToSort = fieldNamesForInputValues[form.sortBy.value];
  const sortOrderOption = sortOrder[form.sortOrder.value];
  return {
    fieldToSort,
    order: sortOrderOption,
  };
}

export function getSearchOptions() {
  const form = document.forms.search;
  return {
    note: form.note.value,
    name: form.name.value,
    ageString: form.age.value,
    getPercentage: form.percentage.checked,
  };
}
