import {
  onSubmitFilter, onSubmitSort, onSubmitSearch, onValidateButtonClicked, onShowButtonClicked,
} from './event_listeners_for_forms_lab3';

window.addEventListener('load', () => {
  document.getElementById('validate-button').addEventListener('click', onValidateButtonClicked);
  document.getElementById('show-button').addEventListener('click', onShowButtonClicked);
  document.forms.filter.onsubmit = onSubmitFilter;
  document.forms.sort.onsubmit = onSubmitSort;
  document.forms.search.onsubmit = onSubmitSearch;
});
