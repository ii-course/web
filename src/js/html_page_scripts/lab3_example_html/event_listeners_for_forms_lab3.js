import { validateUsers } from '../../users_objects_processing/validation/validation';
import users from '../../users_objects_processing/validation/users_validated';
import { getFilterOptions, getSearchOptions, getSortingOptions } from './forms_options_gathering';
import {
  getFilteredUsers,
  getSortedUsers,
  getUsersFound,
} from '../../users_objects_processing/processing/users_objects_processing';
import { outputFilterResults, outputSearchResults, outputSortResults } from './console_output';

export function onShowButtonClicked() {
  console.log(users);
}

export function onValidateButtonClicked() {
  validateUsers(users);
}

export function onSubmitFilter(event) {
  event.preventDefault();
  const options = getFilterOptions();
  const filteredUsers = getFilteredUsers(options);
  outputFilterResults(filteredUsers, options);
}

export function onSubmitSort() {
  // eslint-disable-next-line no-restricted-globals
  event.preventDefault();
  const options = getSortingOptions();
  const sortedUsers = getSortedUsers(options);
  outputSortResults(sortedUsers, options);
}

export function onSubmitSearch() {
  // eslint-disable-next-line no-restricted-globals
  event.preventDefault();
  const options = getSearchOptions();
  const usersFound = getUsersFound(options);
  outputSearchResults(usersFound, options);
}
