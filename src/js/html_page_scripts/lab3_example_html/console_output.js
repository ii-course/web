import { operationSymbols } from '../../users_objects_processing/processing/filters';
import users from '../../users_objects_processing/validation/users_validated';
import getPercentageFromArraysLength from '../../users_objects_processing/processing/maths';

function createOutputForFiltering(options) {
  const operationSymbol = options.ageOperation.length > 0
    ? operationSymbols[options.ageOperation] : operationSymbols.EQUAL;
  const countryOutput = options.country.length > 0 ? `\n> live in ${options.country}` : '';
  const ageOutput = options.age.length > 0 ? `\n> ${operationSymbol} ${options.age} years old` : '';
  const genderOutput = options.gender.length > 0 ? `\n> ${options.gender}` : '';
  const favouriteOutput = options.isFavourite.length > 0
    ? `\n> ${options.isFavourite === 'true' ? '' : 'not '}marked as 'favourite'` : '';
  return `Users${countryOutput}${ageOutput}${genderOutput}${favouriteOutput}`.trim();
}

export function outputFilterResults(filteredUsers, options) {
  const outputString = createOutputForFiltering(options);
  console.log(outputString);
  if (options.getPercentage) {
    const percent = getPercentageFromArraysLength(filteredUsers, users);
    console.log(`${percent}%`);
  } else {
    console.log(filteredUsers);
  }
}

export function outputSortResults(sortedUsers, options) {
  let i = 1;
  sortedUsers.forEach((user) => {
    console.log(`[${i}][Sort: ${options.fieldToSort} ${options.order === 0 ? 'ASC' : 'DESC'}] `
      + `User: ${user.full_name}${options.fieldToSort === 'full_name' ? '' : `, ${options.fieldToSort}: ${user[options.fieldToSort]}`}`);
    i += 1;
  });
}

export function outputSearchResults(usersFound, options) {
  const nameOutput = options.name.length > 0 ? `\n> name '${options.name}'` : '';
  const noteOutput = options.note.length > 0 ? `\n> note '${options.note}'` : '';
  const ageOutput = options.ageString.length > 0 ? `\n> age '${options.ageString}'` : '';
  console.log(`Users found by${nameOutput}${noteOutput}${ageOutput}`);
  if (options.getPercentage) {
    const percent = getPercentageFromArraysLength(usersFound, users);
    console.log(`${percent}%`);
  } else {
    console.log(usersFound);
  }
}
