import resetStatisticsSection from './statistics_table/reset';

export default function addListenerToTitle() {
  const title = document.getElementById('title');
  title.addEventListener('click', resetStatisticsSection);
}
