import { PREV_PAGE_BUTTON, CURRENT_PAGE_BUTTON, HIGHLIGHTED_BUTTON } from './page_buttons_positions';

function highlightCurrentPageButton() {
  const button = document.getElementById(CURRENT_PAGE_BUTTON.id);
  button?.classList.add(HIGHLIGHTED_BUTTON);
}

function removeHighlightFromPrevPageButton() {
  const button = document.getElementById(PREV_PAGE_BUTTON.id);
  button?.classList.remove(HIGHLIGHTED_BUTTON);
  PREV_PAGE_BUTTON.id = CURRENT_PAGE_BUTTON.id;
}

export default function handleHighlight() {
  highlightCurrentPageButton();
  const sameButtonClicked = CURRENT_PAGE_BUTTON.id === PREV_PAGE_BUTTON.id;
  if (!sameButtonClicked) {
    removeHighlightFromPrevPageButton();
  }
}
