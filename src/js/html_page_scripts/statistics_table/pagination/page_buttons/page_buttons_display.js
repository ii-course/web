import { difference } from 'lodash';
import { hideElement, showElement } from '../../../html_elements_processing';
import { getPagesTotal } from '../pages_calculations';
import {
  allButtonsPositions, CURRENT_PAGE_BUTTON, BEGINNING_SECTION, MIDDLE_SECTION, END_SECTION, FIRST,
  SECOND, THIRD, LEFT_SECTION, RIGHT_SECTION,
} from './page_buttons_positions';
import {
  getButtonBySectionAndPosition, setTextContentToLastButton,
  isPageInTheEnd, isPageInTheBeginning,
} from './page_buttons_processing';

function getEllipsisBySection(section) {
  return document.getElementById(`pagination-ellipsis-${section}`);
}

function hideEllipsis(section) {
  const ellipsis = getEllipsisBySection(section);
  hideElement(ellipsis);
}

function showEllipsis(section) {
  const ellipsis = getEllipsisBySection(section);
  showElement(ellipsis);
}

function hidePageButton(section, positionNumber) {
  const button = getButtonBySectionAndPosition(section, positionNumber);
  hideElement(button);
}

function showPageButton(section, positionNumber) {
  const button = getButtonBySectionAndPosition(section, positionNumber);
  showElement(button);
}

function showButtonsByGivenPositions(positions) {
  positions.forEach((position) => showPageButton(position[0], position[1]));
}

function hideButtonsByGivenPositions(positions) {
  positions.forEach((position) => hidePageButton(position[0], position[1]));
}

function getBeginningButtonsPositions() {
  // 1 <2> 3 ... Last
  const buttonsPositions = [];
  const pagesTotal = getPagesTotal();
  if (pagesTotal >= 1) { // 1
    buttonsPositions.push([BEGINNING_SECTION, FIRST]);
  }
  if (pagesTotal >= 2) { // 1 2
    buttonsPositions.push([BEGINNING_SECTION, SECOND]);
  }
  if (pagesTotal >= 3) { // 1 2 3
    buttonsPositions.push([BEGINNING_SECTION, THIRD]);
  }
  if (pagesTotal > 3) { // 1 <2> 3 ... Last
    buttonsPositions.push([END_SECTION, THIRD]);
    setTextContentToLastButton('Last');
  }
  return buttonsPositions;
}

function getEndButtonsPositions() {
  // 1 ... n-2 <n-1> n
  return [[BEGINNING_SECTION, FIRST],
    [END_SECTION, FIRST], [END_SECTION, SECOND], [END_SECTION, THIRD]];
}

function getMiddleButtonsPositions() {
  // 1 ... 4 <5> 6 ... Last
  return [[BEGINNING_SECTION, FIRST],
    [MIDDLE_SECTION, FIRST], [MIDDLE_SECTION, SECOND], [MIDDLE_SECTION, THIRD],
    [END_SECTION, THIRD]];
}

function getPageButtonsToShow(page) {
  /*
 1 2 3 ... [] [] [] ... [] [] []
 3 cases:
 1 <2> 3 ... Last
 1 ... n-2 <n-1> n
 1 ... 4 <5> 6 ... Last
  */
  const pagesTotal = getPagesTotal();
  if (isPageInTheBeginning(page) || pagesTotal < 4) {
    return getBeginningButtonsPositions();
  }
  if (isPageInTheEnd(page)) {
    return getEndButtonsPositions();
  }
  return getMiddleButtonsPositions(page);
}

export function handleEllipsis() {
  const page = CURRENT_PAGE_BUTTON.number;
  const pagesTotal = getPagesTotal();
  if (pagesTotal < 4) { // 1 2 3
    hideEllipsis(LEFT_SECTION);
    hideEllipsis(RIGHT_SECTION);
  } else if (isPageInTheBeginning(page)) { // 1 2 3 ... Last
    showEllipsis(LEFT_SECTION);
    hideEllipsis(RIGHT_SECTION);
  } else if (isPageInTheEnd(page)) { // 1 ... n-2 <n-1> n
    hideEllipsis(LEFT_SECTION);
    showEllipsis(RIGHT_SECTION);
  } else { // 1 ... 4 <5> 6 ... Last
    showEllipsis(LEFT_SECTION);
    showEllipsis(RIGHT_SECTION);
  }
}

export function handlePageButtonsDisplay() {
  const buttonsToShow = getPageButtonsToShow(CURRENT_PAGE_BUTTON.number);
  const buttonsToHide = difference(allButtonsPositions, buttonsToShow);
  hideButtonsByGivenPositions(buttonsToHide);
  showButtonsByGivenPositions(buttonsToShow);
}
