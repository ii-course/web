import { getPagesTotal } from '../pages_calculations';
import { END_SECTION, THIRD } from './page_buttons_positions';

export function isPageInTheBeginning(page) {
  return page < 3;
}

export function isPageInTheEnd(page) {
  const pagesTotal = getPagesTotal();
  return page > pagesTotal - 2;
}

export function isPageInTheMiddleStrictly(page) {
  return !isPageInTheBeginning(page) && !isPageInTheEnd(page);
}

export function getSectionFromPageButton(button) {
  // pagination-button-{section}-{positionNumber}
  const { id } = button;
  return id.split('-')[2];
}

export function getPositionNumberFromPageButton(button) {
  // pagination-button-{section}-{positionNumber}
  const { id } = button;
  return id.split('-')[3];
}

export function getPageNumberFromButton(button) {
  return parseInt(button.dataset.pageNumber, 10);
}

function setPageNumberInDataAttr(button, pageNumber) {
  // eslint-disable-next-line no-param-reassign
  button.dataset.pageNumber = pageNumber.toString();
}

export function getAllPagesButtons() {
  return [...document.getElementsByClassName('pagination-button')];
}

export function getIdForPageButton(section, positionNumber) {
  return `pagination-button-${section}-${positionNumber}`;
}

export function getButtonBySectionAndPosition(section, positionNumber) {
  const buttonId = getIdForPageButton(section, positionNumber);
  return document.getElementById(buttonId);
}

export function setTextContentToLastButton(text) {
  const lastButton = getButtonBySectionAndPosition(END_SECTION, THIRD);
  lastButton.textContent = text;
}

export function addPageNumberToButton(section, positionNumber, pageNumber) {
  const button = getButtonBySectionAndPosition(section, positionNumber);
  button.textContent = pageNumber.toString();
  setPageNumberInDataAttr(button, pageNumber);
}

export function setLastButton(textContext) {
  const lastButton = getButtonBySectionAndPosition(END_SECTION, THIRD);
  lastButton.textContent = textContext.toString();
  const pagesTotal = getPagesTotal();
  lastButton.dataset.pageNumber = pagesTotal.toString();
}
