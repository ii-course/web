import { getPagesTotal } from '../pages_calculations';
import {
  CURRENT_PAGE_BUTTON, BEGINNING_SECTION, MIDDLE_SECTION, END_SECTION,
  FIRST, SECOND, THIRD,
} from './page_buttons_positions';
import {
  getPageNumberFromButton, getSectionFromPageButton, getPositionNumberFromPageButton,
  isPageInTheMiddleStrictly, isPageInTheBeginning, isPageInTheEnd, getIdForPageButton,
} from './page_buttons_processing';

function remainsInTheMiddle(button) {
  const section = getSectionFromPageButton(button);
  const positionNumber = getPositionNumberFromPageButton(button);
  const pageNumber = getPageNumberFromButton(button);
  const pagesTotal = getPagesTotal();
  return (section === BEGINNING_SECTION && positionNumber === THIRD && pagesTotal > 3)
    || (section === END_SECTION && positionNumber === FIRST)
    || (section === MIDDLE_SECTION && isPageInTheMiddleStrictly(pageNumber));
}

function movesToTheBeginning(button) {
  const section = getSectionFromPageButton(button);
  const pageNumber = getPageNumberFromButton(button);
  return section === MIDDLE_SECTION && isPageInTheBeginning(pageNumber);
}

function movesToTheEnd(button) {
  const section = getSectionFromPageButton(button);
  const pageNumber = getPageNumberFromButton(button);
  return section === MIDDLE_SECTION && isPageInTheEnd(pageNumber);
}

function getPageButtonIdInTheBeginning(button) {
  const pageNumber = getPageNumberFromButton(button);
  const isSecond = pageNumber === 2;
  if (isSecond) {
    return getIdForPageButton(BEGINNING_SECTION, SECOND);
  }
  if (pageNumber === 1) {
    return getIdForPageButton(BEGINNING_SECTION, FIRST);
  }
  return button.id;
}

function getPageButtonIdInTheEnd(button) {
  const pageNumber = getPageNumberFromButton(button);
  const pagesTotal = getPagesTotal();
  const isSecondToLast = pageNumber === pagesTotal - 1;
  if (isSecondToLast) {
    return getIdForPageButton(END_SECTION, SECOND);
  }
  if (pageNumber === pagesTotal) {
    return getIdForPageButton(END_SECTION, THIRD);
  }
  return button.id;
}

export default function setCurrentPageId(newCurrentButton) {
  if (remainsInTheMiddle(newCurrentButton)) {
    CURRENT_PAGE_BUTTON.id = getIdForPageButton(MIDDLE_SECTION, SECOND);
  } else if (movesToTheBeginning(newCurrentButton)) {
    CURRENT_PAGE_BUTTON.id = getPageButtonIdInTheBeginning(newCurrentButton);
  } else if (movesToTheEnd(newCurrentButton)) {
    CURRENT_PAGE_BUTTON.id = getPageButtonIdInTheEnd(newCurrentButton);
  } else {
    CURRENT_PAGE_BUTTON.id = newCurrentButton.id;
  }
}
