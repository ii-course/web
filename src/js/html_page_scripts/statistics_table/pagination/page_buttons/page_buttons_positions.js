export const CURRENT_PAGE_BUTTON = {
  number: 1,
  id: 'pagination-button-beginning-first',
};

export const PREV_PAGE_BUTTON = {
  id: 'pagination-button-beginning-first',
};

export const HIGHLIGHTED_BUTTON = 'pagination-button-current';

// @TODO: make const objects

export const BEGINNING_SECTION = 'beginning';
export const MIDDLE_SECTION = 'middle';
export const END_SECTION = 'end';

export const LEFT_SECTION = 'left';
export const RIGHT_SECTION = 'right';

export const FIRST = 'first';
export const SECOND = 'second';
export const THIRD = 'third';

export const allButtonsPositions = [
  [BEGINNING_SECTION, FIRST], [BEGINNING_SECTION, SECOND], [BEGINNING_SECTION, THIRD],
  [MIDDLE_SECTION, FIRST], [MIDDLE_SECTION, SECOND], [MIDDLE_SECTION, THIRD],
  [END_SECTION, FIRST], [END_SECTION, SECOND], [END_SECTION, THIRD]];
