import showUsersOnPage from '../rewriting_data';
import { handleEllipsis, handlePageButtonsDisplay } from './page_buttons/page_buttons_display';
import handleHighlight from './page_buttons/current_page_button_highlight';
import setCurrentPageId from './page_buttons/page_buttons_id';
import addPagesNumbersToButtons from './page_numbers_display';
import { BEGINNING_SECTION, CURRENT_PAGE_BUTTON, FIRST } from './page_buttons/page_buttons_positions';
import {
  getPageNumberFromButton,
  getAllPagesButtons,
  getButtonBySectionAndPosition,
} from './page_buttons/page_buttons_processing';
import { getPagesTotal } from './pages_calculations';

function showPagesButtons() {
  handlePageButtonsDisplay();
  handleEllipsis();
  addPagesNumbersToButtons();
  handleHighlight();
}

function getPageNumberValidated(page) {
  if (page <= 0) {
    return 1;
  }
  const pagesTotal = getPagesTotal();
  if (page > pagesTotal) {
    return pagesTotal;
  }
  return page;
}

function goToPage(page = 1) {
  const pageValidated = getPageNumberValidated(page);
  CURRENT_PAGE_BUTTON.number = pageValidated;
  if (pageValidated === 1) {
    setCurrentPageId(getButtonBySectionAndPosition(BEGINNING_SECTION, FIRST));
  }
  showPagesButtons();
  showUsersOnPage(pageValidated);
}

function goToPageWrittenInTheButton(event) {
  const buttonClicked = event.currentTarget;
  setCurrentPageId(buttonClicked);
  const pageNumber = getPageNumberFromButton(buttonClicked);
  goToPage(pageNumber);
}

function addListenersToPagesButtons() {
  const buttons = getAllPagesButtons();
  buttons.forEach((button) => button.addEventListener('click', goToPageWrittenInTheButton));
}

export { goToPage, addListenersToPagesButtons };
