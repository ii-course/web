import { getPagesTotal } from './pages_calculations';
import {
  addPageNumberToButton,
  isPageInTheBeginning, isPageInTheEnd, setLastButton,
} from './page_buttons/page_buttons_processing';
import {
  CURRENT_PAGE_BUTTON, MIDDLE_SECTION, END_SECTION,
  FIRST, SECOND, THIRD,
} from './page_buttons/page_buttons_positions';

function addPagesNumbersToEndButtons() {
  // 1 ... n-2 <n-1> n
  const pagesTotal = getPagesTotal();
  addPageNumberToButton(END_SECTION, FIRST, pagesTotal - 2);
  addPageNumberToButton(END_SECTION, SECOND, pagesTotal - 1);
}

function addPagesNumbersToMiddleButtons(page) {
  // 1 ... 4 <5> 6 ... Last
  addPageNumberToButton(MIDDLE_SECTION, FIRST, page - 1);
  addPageNumberToButton(MIDDLE_SECTION, SECOND, page);
  addPageNumberToButton(MIDDLE_SECTION, THIRD, page + 1);
}

export default function addPagesNumbersToButtons() {
  const page = CURRENT_PAGE_BUTTON.number;
  let textInLastButton;
  if (isPageInTheBeginning(page)) {
    // 1 <2> 3 ... Last
    textInLastButton = 'Last';
  } else if (isPageInTheEnd(page)) {
    // 1 ... n-2 <n-1> n
    const pagesTotal = getPagesTotal();
    addPagesNumbersToEndButtons();
    textInLastButton = pagesTotal.toString();
  } else {
    // 1 ... 4 <5> 6 ... Last
    addPagesNumbersToMiddleButtons(page);
    textInLastButton = 'Last';
  }
  setLastButton(textInLastButton);
}
