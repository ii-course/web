import { users } from '../../current_users_for_statistics/users';

export const USERS_ON_PAGE = 10;

export function getPagesTotal(itemsPerPage = USERS_ON_PAGE, itemsTotal = users.length) {
  let pages = Math.trunc(itemsTotal / itemsPerPage);
  if (itemsTotal % itemsPerPage > 0) {
    pages += 1;
  }
  return pages;
}
