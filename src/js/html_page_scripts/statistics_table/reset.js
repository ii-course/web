import { replaceCurrentUsersWith } from '../current_users_for_statistics/users';
import { users as allUsers } from '../../users_objects_processing/users_for_ui_with_changes_tracked';
import { goToPage } from './pagination/pagination';
import { hideAllCharts } from './chart/chart_visibility';

function resetTable() {
  replaceCurrentUsersWith(allUsers);
  goToPage(1);
}

export default function resetStatisticsSection() {
  resetTable();
  hideAllCharts();
}
