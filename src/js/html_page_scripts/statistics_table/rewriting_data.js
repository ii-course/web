import { users } from '../current_users_for_statistics/users';
import { USERS_ON_PAGE } from './pagination/pages_calculations';
import showTeacherInfoPopup from '../popups/teacher_info/display';
import { appendChildrenToAnElement } from '../html_elements_processing';

function getUsersToShow(pageNumber, allUsers) {
  const beginIndex = (pageNumber - 1) * USERS_ON_PAGE;
  return allUsers.slice(beginIndex, beginIndex + USERS_ON_PAGE);
}

function clearStatisticsTable() {
  const tableBody = document.querySelector('#statistics-table tbody');
  tableBody.innerHTML = '';
}

function createTdNameWithId(user) {
  const tdName = document.createElement('td');
  tdName.classList.add('name');
  tdName.textContent = user.full_name;
  tdName.dataset.id = user.id;
  tdName.addEventListener('click', showTeacherInfoPopup);
  return tdName;
}

function createOrdinaryTdCell(cellName, text) {
  const td = document.createElement('td');
  td.textContent = text;
  td.dataset.cellName = cellName;
  return td;
}

function createUserRowElementForTable(user) {
  const row = document.createElement('tr');
  const textForAgeCell = Number.isNaN(user.age) ? ' ' : user.age.toString();
  appendChildrenToAnElement(row, createTdNameWithId(user), createOrdinaryTdCell('age', textForAgeCell),
    createOrdinaryTdCell('gender', user.gender), createOrdinaryTdCell('country', user.country));
  return row;
}

function getTableRowsWithUsers(usersToAdd) {
  const rows = [];
  usersToAdd.forEach((user) => {
    const row = createUserRowElementForTable(user);
    rows.push(row);
  });
  return rows;
}

function getFragmentWithUsers(usersToAdd) {
  const usersRows = getTableRowsWithUsers(usersToAdd);
  const fragment = document.createDocumentFragment();
  usersRows.forEach((row) => {
    fragment.appendChild(row);
  });
  return fragment;
}

function fillStatisticsTableWithUsers(usersToAdd) {
  const fragment = getFragmentWithUsers(usersToAdd);
  const tableBody = document.querySelector('#statistics-table tbody');
  tableBody.appendChild(fragment);
}

function rewriteStatisticsTableWithUsers(usersToAdd) {
  clearStatisticsTable();
  fillStatisticsTableWithUsers(usersToAdd);
}

export default function showUsersOnPage(pageNumber = 1, allUsers = users) {
  const usersToShow = getUsersToShow(pageNumber, allUsers);
  rewriteStatisticsTableWithUsers(usersToShow);
}
