import { difference } from 'lodash';
import { findAllBy } from '../../../users_objects_processing/processing/search';
import { replaceCurrentUsersWith, users as usersNotFiltered }
  from '../../current_users_for_statistics/users';
import { fetchSearchResults } from '../../current_users_for_statistics/users_update';
import { goToPage } from '../pagination/pagination';
import showChart from '../chart/chart_display';

const numericFields = ['age'];
const fieldsAvailableForFiltering = ['age', 'country', 'gender'];

function getFilterCheckboxes() {
  return [...document.getElementsByClassName('input-filter-statistics-checkbox')];
}

function getFilterInputs() {
  return [...document.getElementsByClassName('inputs-filter-value-statistics')];
}

function getInputIdForField(field) {
  return `${field}-filter-statistics`;
}

function getInputValueForField(field) {
  const inputId = getInputIdForField(field);
  return document.getElementById(inputId)?.value;
}

function getValueForField(field) {
  if (field === 'gender') {
    const checkedRadio = document.querySelector('.gender-radio-filter-statistics:checked');
    if (checkedRadio) {
      return checkedRadio.value;
    }
    return null;
  }
  const rawValue = getInputValueForField(field);
  if (numericFields.includes(field)) {
    return Number.parseInt(rawValue, 10);
  }
  return rawValue;
}

export function getFilterByFields() {
  const checkedCheckboxes = document.querySelectorAll('.input-filter-statistics-checkbox:checked');
  const filterByFields = [];
  checkedCheckboxes.forEach((checkbox) => filterByFields.push(checkbox.value));
  return filterByFields;
}

function getFieldsForChartStatistics() {
  const filterByFields = getFilterByFields();
  return difference(fieldsAvailableForFiltering, filterByFields);
}

function getUsersFiltered() {
  fetchSearchResults();
  let usersFilteredByAllFields = [...usersNotFiltered];
  const filterByFields = getFilterByFields();
  if (filterByFields.length > 0) {
    filterByFields.forEach((field) => {
      const value = getValueForField(field);
      if (value) {
        usersFilteredByAllFields = findAllBy(usersFilteredByAllFields, field, value);
      }
    });
  }
  return usersFilteredByAllFields;
}

function showFilterResults() {
  const usersFiltered = getUsersFiltered();
  const fieldsForChart = getFieldsForChartStatistics();
  showChart(usersFiltered, fieldsForChart);
  replaceCurrentUsersWith(usersFiltered);
  goToPage(1);
}

function clearCheckboxes() {
  const checkboxes = getFilterCheckboxes();
  for (let i = 0; i < checkboxes.length; i += 1) {
    checkboxes[i].checked = false;
  }
}

function clearInputs() {
  const inputs = getFilterInputs();
  for (let i = 0; i < inputs.length; i += 1) {
    const isRadio = inputs[i].classList.contains('gender-radio-filter-statistics');
    if (!isRadio) {
      inputs[i].value = '';
    }
  }
}

function clearRadioButtons() {
  const checkedRadio = document.querySelector('.gender-radio-filter-statistics:checked');
  if (checkedRadio) {
    checkedRadio.checked = false;
  }
}

function addListenersToCheckboxesForFiltering() {
  const checkboxes = getFilterCheckboxes();
  checkboxes.forEach((checkbox) => {
    checkbox.addEventListener('click', showFilterResults);
  });
}

function getCheckboxByName(name) {
  return document.getElementById(`${name}-filter-checkbox`);
}

function addListenersToCheckboxesIfAreUnchecked() {
  const checkboxesWithActions = {
    gender: clearRadioButtons,
    age: () => {
      document.getElementById('age-filter-statistics').value = '';
    },
    country: () => {
      document.getElementById('country-filter-statistics').value = '';
    },
  };
  Object.entries(checkboxesWithActions).forEach(([checkboxName, action]) => {
    const checkbox = getCheckboxByName(checkboxName);
    checkbox.addEventListener('click', (event) => {
      const currentCheckbox = event.currentTarget;
      if (!currentCheckbox.checked) {
        action();
      }
    });
  });
}

function addListenersToCheckboxes() {
  addListenersToCheckboxesForFiltering();
  addListenersToCheckboxesIfAreUnchecked();
}

function addListenersToInputs() {
  const inputs = getFilterInputs();
  inputs.forEach((input) => input.addEventListener('input', showFilterResults));
}

export function clearFilters() {
  clearCheckboxes();
  clearInputs();
  clearRadioButtons();
}

export function addFiltersToStatisticsTable() {
  addListenersToCheckboxes();
  addListenersToInputs();
}
