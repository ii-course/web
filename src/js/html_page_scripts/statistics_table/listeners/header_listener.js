import resetStatisticsSection from '../reset';
import { clearFilters } from './filter_listeners';
import { rewriteUsers } from '../../current_users_for_statistics/users';
import clearSearchLine from '../../search/clear';

export default function addResetListenerToHeader() {
  const header = document.getElementById('statistics-table-header');
  header.addEventListener('click', () => {
    clearFilters();
    resetStatisticsSection();
    rewriteUsers();
    clearSearchLine();
  });
}
