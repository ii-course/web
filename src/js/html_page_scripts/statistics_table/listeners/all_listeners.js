import { addListenersToPagesButtons } from '../pagination/pagination';
import { addFiltersToStatisticsTable } from './filter_listeners';
import addResetListenerToHeader from './header_listener';
import addSortToTableHeaders from './sort_listeners';

export default function addListenersToStatisticsTable() {
  addListenersToPagesButtons();
  addFiltersToStatisticsTable();
  addResetListenerToHeader();
  addSortToTableHeaders();
}
