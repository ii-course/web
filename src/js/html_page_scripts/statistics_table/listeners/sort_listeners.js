import { sortOrder as sortOrderParams, sortUsersBy }
  from '../../../users_objects_processing/processing/sort';
import { replaceCurrentUsersWith, users as usersNotSorted }
  from '../../current_users_for_statistics/users';
import { goToPage } from '../pagination/pagination';
import { CURRENT_PAGE_BUTTON } from '../pagination/page_buttons/page_buttons_positions';

function getHeaderElementByName(headerName) {
  return document.getElementById(`statistics-table-${headerName}`);
}

function getAllHeadersElements() {
  const headerNames = ['name', 'age', 'gender', 'country'];
  const headersElements = [];
  headerNames.forEach((headerName) => {
    const headerElement = getHeaderElementByName(headerName);
    headersElements.push(headerElement);
  });
  return headersElements;
}

function getNextSortOrderOption(currentSortOrder) {
  if (currentSortOrder === sortOrderParams.ASC) {
    return sortOrderParams.DESC;
  }
  return sortOrderParams.ASC;
}

function getUsersSorted(sortBy, sortOrder) {
  return sortUsersBy(usersNotSorted, sortBy, sortOrder);
}

function getSortOrder(header) {
  return Number.parseInt(header.dataset.sortOrder, 10);
}

function getSortBy(header) {
  return header.dataset.sortBy;
}

function setNextSortOrder(headerId) {
  const header = document.getElementById(headerId);
  const sortOrder = getSortOrder(header);
  header.dataset.sortOrder = getNextSortOrderOption(sortOrder).toString();
}

function handleSortingForColumn(event) {
  const header = event.currentTarget;
  setNextSortOrder(header.id);
  const sortOrder = getSortOrder(header);
  const sortBy = getSortBy(header);
  const usersSorted = getUsersSorted(sortBy, sortOrder);
  replaceCurrentUsersWith(usersSorted);
  goToPage(CURRENT_PAGE_BUTTON.number);
}

export default function addSortToTableHeaders() {
  const headersElements = getAllHeadersElements();
  headersElements.forEach((headerElement) => {
    headerElement.addEventListener('click', handleSortingForColumn);
  });
}
