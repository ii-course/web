import { capitalize, groupBy, map } from 'lodash';
import { makeChartVisible, hideChartsStartingFromIndex } from './chart_visibility';
import fillChart from './chart_drawing';
import getRandomColour from './colours';
import { getChartId, getChartContainerId } from './elements_getter';

function getLabelsWithCount(users, field) {
  const groupedBy = groupBy(users, (user) => user[field]);
  return map(groupedBy, (items, label) => ({
    label,
    count: items.length,
  }));
}

function getDataForField(users, field) {
  const labelsWithCount = getLabelsWithCount(users, field);
  const data = {
    labels: [],
    datasets: [],
  };
  const dataset = {
    label: '',
    data: [],
    backgroundColor: [],
    hoverOffset: 4,
  };
  labelsWithCount.forEach((labelWithCount) => {
    data.labels.push(labelWithCount.label);
    dataset.data.push(labelWithCount.count);
    dataset.backgroundColor.push(getRandomColour());
  });
  data.datasets.push(dataset);
  return data;
}

function display(chartNumber, data, options) {
  const chartId = getChartId(chartNumber);
  const chartContainerId = getChartContainerId(chartNumber);
  fillChart(chartId, data, options);
  makeChartVisible(chartContainerId, true);
}

function getOptionsForTitle(titleText) {
  return {
    title: {
      display: true,
      text: capitalize(titleText),
    },
  };
}

export default function showChart(users, fieldsForStatisticsDisplay) {
  for (let i = 0; i < fieldsForStatisticsDisplay.length; i += 1) {
    const field = fieldsForStatisticsDisplay[i];
    const data = getDataForField(users, field);
    const options = getOptionsForTitle(field);
    display(i, data, options);
  }
  hideChartsStartingFromIndex(fieldsForStatisticsDisplay.length);
}
