export default function rewriteData(chart, newData, newOptions) {
  // eslint-disable-next-line no-param-reassign
  chart.data = newData;
  // eslint-disable-next-line no-param-reassign
  chart.options = newOptions;
  chart.update();
}
