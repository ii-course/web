import { getChartContainerId } from './elements_getter';

export function makeChartVisible(chartContainerId, makeVisible) {
  const chartContainer = document.getElementById(chartContainerId);
  chartContainer.style.display = makeVisible ? 'block' : 'none';
}

export function hideChartsStartingFromIndex(index) {
  for (let i = index; i < 3; i += 1) {
    const chartContainerId = getChartContainerId(i);
    makeChartVisible(chartContainerId, false);
  }
}

export function hideAllCharts() {
  hideChartsStartingFromIndex(0);
}
