const COLOURS = [
  '#cc4836',
  '#f75c49',
  '#f99589',
  '#65a3be',
  '#548eaa',
  '#23647d',
];

let currentColourIndex = -1;

function resetColourIndex() {
  currentColourIndex = 0;
}

export default function getNextColour() {
  currentColourIndex += 1;
  if (currentColourIndex >= COLOURS.length) {
    resetColourIndex();
  }
  return COLOURS[currentColourIndex];
}
