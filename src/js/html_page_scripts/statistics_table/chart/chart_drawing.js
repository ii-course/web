import Chart from 'chart.js';
import rewriteData from './chart_updating';
import { charts, TYPE } from './constants';

function getContext(chartId) {
  return document.getElementById(chartId).getContext('2d');
}

function initializeChart(chartId, data, options) {
  const config = {
    type: TYPE,
    data,
  };
  const context = getContext(chartId);
  charts[chartId] = new Chart(context, config, options);
}

export default function fillChart(chartId, data, options) {
  if (!charts[chartId]) {
    initializeChart(chartId, data, options);
  } else {
    rewriteData(charts[chartId], data, options);
  }
}
