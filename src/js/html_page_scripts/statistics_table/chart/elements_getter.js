export function getChartId(chartNumber) {
  return `chart${chartNumber}`;
}

export function getChartContainerId(chartNumber) {
  return `${getChartId(chartNumber)}-container`;
}
