import addListenersToPopups from './popups/listeners_adding';
import { addListenersToLineGalleryButtons } from './line_gallery/scroll';
import addListenersToButtons from './buttons_listeners';
import { addListenersToTopTeachersGallery } from './top_teachers_gallery/filters';
import addListenersToStatisticsTable from './statistics_table/listeners/all_listeners';
import addListenerToSearchLine from './search/search_line_listener';
import addListenerToTitle from './title_listener';

export default function addListeners() {
  addListenerToTitle();
  addListenerToSearchLine();
  addListenersToButtons();
  addListenersToTopTeachersGallery();
  addListenersToStatisticsTable();
  addListenersToLineGalleryButtons();
  addListenersToPopups();
}
