import { findAllBy } from '../../users_objects_processing/processing/search';
import { users } from '../../users_objects_processing/users_for_ui_with_changes_tracked';
import { getInputLine, getSelect } from './search_input_getter';

const numericFields = ['age'];

function getSearchBy() {
  return getSelect().value;
}

function getValueToFind(searchBy) {
  let valueToFind = getInputLine().value;
  if (numericFields.includes(searchBy)) {
    valueToFind = Number.parseInt(valueToFind, 10);
  }
  return valueToFind;
}

export default function getUsersFound() {
  const searchBy = getSearchBy();
  const valueToFind = getValueToFind(searchBy);
  if (valueToFind) {
    return findAllBy(users, searchBy, valueToFind);
  }
  return null;
}
