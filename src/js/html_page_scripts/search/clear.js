import { getInputLine } from './search_input_getter';

export default function clearSearchLine() {
  getInputLine().value = '';
}
