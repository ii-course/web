import { replaceCurrentUsersWith } from '../current_users_for_statistics/users';
import { goToPage } from '../statistics_table/pagination/pagination';
import getUsersFound from './users_finder';
import { clearFilters } from '../statistics_table/listeners/filter_listeners';
import { hideAllCharts } from '../statistics_table/chart/chart_visibility';

function showUsersFound(event) {
  event.preventDefault();
  const usersFound = getUsersFound();
  if (usersFound) {
    clearFilters();
    replaceCurrentUsersWith(usersFound);
    goToPage(1);
  } else {
    alert('Cannot perform search. Probably the value entered is in invalid format');
  }
}

export default function addListenerToSearchLine() {
  const searchForm = document.getElementById('search-form');
  searchForm.addEventListener('submit', showUsersFound);
  searchForm.addEventListener('submit', (event) => {
    event.preventDefault();
    hideAllCharts();
  });
}
