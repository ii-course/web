export function getInputLine() {
  return document.getElementById('search-line');
}

export function getSelect() {
  return document.getElementById('search-select');
}
