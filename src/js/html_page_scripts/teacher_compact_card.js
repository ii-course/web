import { appendChildrenToAnElement, createImg } from './html_elements_processing';
import showTeacherInfoPopup from './popups/teacher_info/display';
import { getFirstLetter } from '../strings_processing';

const SRC_FIELD = 'picture_large';
const STAR_ICON_CLASS = 'teacher-card-compact-star';

function getFullNameSplit(fullName) {
  return fullName.trim().split(' ');
}

function containsFirstName(fullName) {
  return getFullNameSplit(fullName).length > 0;
}

function containsLastName(fullName) {
  return getFullNameSplit(fullName).length > 1;
}

function getFirstName(fullName) {
  if (containsFirstName(fullName)) {
    return getFullNameSplit(fullName)[0];
  }
  return '';
}

function getLastName(fullName) {
  if (containsLastName(fullName)) {
    return getFullNameSplit(fullName)[1];
  }
  return '';
}

function getInitials(fullName) {
  const firstName = getFirstName(fullName);
  const lastName = getLastName(fullName);
  return `${getFirstLetter(firstName)}. ${getFirstLetter(lastName)}`;
}

function getStarIcon() {
  const star = document.createElement('i');
  star.classList.add(STAR_ICON_CLASS, 'fas', 'fa-star');
  return star;
}

function getStarIconFromCard(card) {
  const { children } = card;
  for (let i = 0; i < children.length; i += 1) {
    if (children[i].classList.contains(STAR_ICON_CLASS)) {
      return children[i];
    }
  }
  return null;
}

function handleStarIcon(card, isFavourite) {
  const star = getStarIconFromCard(card);
  if (star && isFavourite) {
    star.style.display = null;
  } else if (star) {
    star.style.display = 'none';
  }
}

function getImgContainer(teacher) {
  const imgContainer = document.createElement('div');
  imgContainer.classList.add('card-compact-img-container');
  let imgContainerChild;
  const src = teacher[SRC_FIELD];
  if (src) {
    imgContainerChild = createImg(src, 'Teacher photo');
  } else {
    imgContainerChild = document.createElement('span');
    imgContainerChild.textContent = getInitials(teacher.full_name);
  }
  imgContainer.appendChild(imgContainerChild);
  return imgContainer;
}

function getNameContainer(teacher) {
  const nameContainer = document.createElement('div');
  nameContainer.classList.add('name');
  const firstName = document.createElement('p');
  firstName.textContent = getFirstName(teacher.full_name);
  const lastName = document.createElement('p');
  lastName.textContent = getLastName(teacher.full_name);
  appendChildrenToAnElement(nameContainer, firstName, lastName);
  return nameContainer;
}

function getCountyElement(teacher) {
  const countryElement = document.createElement('p');
  countryElement.textContent = teacher.country;
  return countryElement;
}

function addStarIconToCard(card, isFavourite) {
  const star = getStarIcon();
  card.appendChild(star);
  handleStarIcon(card, isFavourite);
}

function getCardElement(teacher) {
  const card = document.createElement('div');
  card.classList.add('teacher-card-compact');
  card.dataset.id = teacher.id;
  card.addEventListener('click', showTeacherInfoPopup);
  addStarIconToCard(card, teacher.favorite);
  return card;
}

function getCompactCardForTeacher(teacher) {
  const imgContainer = getImgContainer(teacher);
  const nameContainer = getNameContainer(teacher);
  const countryElement = getCountyElement(teacher);
  const card = getCardElement(teacher);
  appendChildrenToAnElement(card, imgContainer, nameContainer, countryElement);
  return card;
}

function getTeachersCards(teachers) {
  return teachers.map((teacher) => getCompactCardForTeacher(teacher));
}

export {
  SRC_FIELD, getCompactCardForTeacher, getTeachersCards, handleStarIcon,
};
