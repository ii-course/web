import { users as usersValidated } from '../../users_objects_processing/validation/users_validated';

let usersPrevCopy = [];
// eslint-disable-next-line import/no-mutable-exports
export let users = [];

export function rewriteUsers() {
  users = [...usersValidated];
  usersPrevCopy = [...usersValidated];
}

export function replaceCurrentUsersWith(newUsers) {
  usersPrevCopy = [...users];
  users = [...newUsers];
}

export function setUsersToPrevCopy() {
  replaceCurrentUsersWith(usersPrevCopy);
}

export function resetUsers() {
  replaceCurrentUsersWith(usersValidated);
}
