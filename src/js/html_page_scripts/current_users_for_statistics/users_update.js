import { replaceCurrentUsersWith } from './users';
import getUsersFound from '../search/users_finder';
import { users as usersUpToDate } from '../../users_objects_processing/users_for_ui_with_changes_tracked';

export function fetchSearchResults() {
  const usersFound = getUsersFound();
  if (usersFound) {
    replaceCurrentUsersWith(usersFound);
  } else {
    replaceCurrentUsersWith(usersUpToDate);
  }
}

export function fetchChanges() {
  replaceCurrentUsersWith(usersUpToDate);
}
