import { showTeacherFormPopup } from './popups/teacher_form/display';

function addListenersToAddTeacherButtons() {
  const addTeacherButtons = document.getElementsByClassName('add-teacher-button');
  Array.from(addTeacherButtons).forEach((button) => button.addEventListener('click', showTeacherFormPopup));
}

export { addListenersToAddTeacherButtons as default };
