export function appendChildrenToAnElement(element, ...children) {
  for (let i = 0; i < children.length; i += 1) {
    element.appendChild(children[i]);
  }
}

export function createImg(src, alt) {
  const img = document.createElement('img');
  img.setAttribute('src', src);
  img.setAttribute('alt', alt);
  return img;
}

export function getUserIdFromElement(element) {
  return element?.dataset.id;
}

export function hideElement(element) {
  // eslint-disable-next-line no-param-reassign
  element.style.display = 'none';
}

export function showElement(element, displayStyle = null) {
  // eslint-disable-next-line no-param-reassign
  element.style.display = displayStyle;
}
