import { getFavouriteTeachers } from '../../users_objects_processing/processing/users_objects_processing';
import { getTeachersCards } from '../teacher_compact_card';
import { setLineGalleryButtonsAvailability } from './scroll';

function getGalleryCardsContainer() {
  return document.getElementById('line-gallery-cards-container');
}

function getCardsForFavouriteTeachers() {
  const teachers = getFavouriteTeachers();
  return getTeachersCards(teachers);
}

function getFragmentForGallery() {
  const teacherCards = getCardsForFavouriteTeachers();
  const fragment = document.createDocumentFragment();
  teacherCards.forEach((card) => fragment.appendChild(card));
  return fragment;
}

function clearGallery() {
  const cardsContainer = getGalleryCardsContainer();
  cardsContainer.innerHTML = '';
}

function fillGallery() {
  const fragment = getFragmentForGallery();
  const cardsContainer = getGalleryCardsContainer();
  cardsContainer.appendChild(fragment);
  setLineGalleryButtonsAvailability();
}

export default function showFavouriteTeachers() {
  clearGallery();
  fillGallery();
}
