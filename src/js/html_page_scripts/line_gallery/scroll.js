const BETWEEN_COMPACT_CARDS = 80;

function disableButton(id, setDisabled) {
  document.getElementById(id).disabled = setDisabled;
}

function getLineGalleryById() {
  return document.getElementById('line-gallery-cards-container');
}

function getLineGalleryWidth() {
  const lineGallery = getLineGalleryById();
  const widthString = window.getComputedStyle(lineGallery).getPropertyValue('width'); // px
  const widthStringWithoutPx = widthString.slice(0, widthString.length - 2);
  return Number.parseInt(widthStringWithoutPx, 10);
}

function getCardWidth() {
  const teacherCardCompactImgWidth = 112;
  const teacherCardCompactImgBorderWidth = 4;
  return teacherCardCompactImgWidth + teacherCardCompactImgBorderWidth * 2;
}

function getCardWidthWithMargin() {
  return getCardWidth() + BETWEEN_COMPACT_CARDS;
}

function getNumOfCardsVisible() {
  const cardWidth = getCardWidth();
  let lineGalleryWidth = getLineGalleryWidth();
  let numOfCardsVisible = 0;
  while (lineGalleryWidth > 0) {
    lineGalleryWidth -= (cardWidth + BETWEEN_COMPACT_CARDS);
    numOfCardsVisible += 1;
  }
  return numOfCardsVisible;
}

function getMaxRight() {
  const lineGallery = getLineGalleryById();
  const cardWidthWithMargin = getCardWidthWithMargin();
  const countCards = lineGallery.children.length;
  const numOfCardsVisible = getNumOfCardsVisible();
  const defaultMaxRight = cardWidthWithMargin * (countCards - numOfCardsVisible);
  const moreCardsTotalThanShown = defaultMaxRight > 0;
  return moreCardsTotalThanShown ? defaultMaxRight : 0;
}

function getCurrentLeftPositionOfGallery() {
  const lineGallery = getLineGalleryById();
  return Number.parseInt(window.getComputedStyle(lineGallery).getPropertyValue('left'), 10);
}

function setLeftButtonAvailability(currentLeft) {
  const setLeftButtonDisabled = currentLeft >= 0;
  disableButton('button-left', setLeftButtonDisabled);
}

function setRightButtonAvailability(currentLeft) {
  const currentRight = -currentLeft;
  const maxRight = getMaxRight();
  const setRightButtonDisabled = currentRight >= maxRight;
  disableButton('button-right', setRightButtonDisabled);
}

function setLineGalleryLeftPosition(position) {
  const lineGallery = getLineGalleryById();
  lineGallery.style.left = `${position}px`;
}

export function setLineGalleryButtonsAvailability(currentLeft = getCurrentLeftPositionOfGallery()) {
  setLeftButtonAvailability(currentLeft);
  setRightButtonAvailability(currentLeft);
}

function setLineGalleryLeftPositionSafety(position = getCurrentLeftPositionOfGallery()) {
  setLineGalleryLeftPosition(position);
  setLineGalleryButtonsAvailability(position);
}

export function addListenersToLineGalleryButtons() {
  let currentLeft = getCurrentLeftPositionOfGallery();
  setLineGalleryButtonsAvailability(currentLeft);

  const buttonLeft = document.getElementById('button-left');
  const buttonRight = document.getElementById('button-right');
  const SHIFT = getCardWidthWithMargin();

  buttonLeft.addEventListener('click', () => {
    currentLeft += SHIFT;
    setLineGalleryLeftPositionSafety(currentLeft);
  });

  buttonRight.addEventListener('click', () => {
    currentLeft -= SHIFT;
    setLineGalleryLeftPositionSafety(currentLeft);
  });

  window.addEventListener('resize', () => {
    setLineGalleryButtonsAvailability();
  });
}
