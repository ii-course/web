export function isLetterInUpperCase(letter) {
  return letter === letter.toUpperCase();
}

export function getFirstLetter(string) {
  return string.length > 0 ? string[0] : '';
}

export function isArabic(string) {
  const arabic = /[\u0600-\u06FF]/;
  const stringWithoutSpaces = string.split(' ').join('');
  return arabic.test(stringWithoutSpaces);
}
