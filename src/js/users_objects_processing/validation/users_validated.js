import { validateUsers } from './validation';
import { SEED } from '../api/url_constants';
import { changeAllUsersObjectsToNewFormat } from '../modification/users_objects_modifier';
import getUsersRawData from '../api/users_api_getter';

// eslint-disable-next-line import/no-mutable-exports
let users = []; // users = getFormattedAndMergedUsersArray();
const PARAMS = {
  seed: SEED,
  results: 50,
};

function getUsersValidated(params) {
  return getUsersRawData(params)
    .then((response) => {
      if (response) {
        const usersFromResponse = changeAllUsersObjectsToNewFormat(response.results);
        validateUsers(usersFromResponse);
        return usersFromResponse;
      }
      return [];
    })
    .catch((error) => console.error(error));
}

function getUsersFromSeedValidated(optionalParams) {
  const paramsToSend = Object.assign(PARAMS, optionalParams);
  return getUsersValidated(paramsToSend);
}

function setUsers() {
  return getUsersFromSeedValidated()
    .then((usersFromResponse) => {
      users = usersFromResponse;
    })
    .catch((error) => console.error(error));
}

export {
  users, getUsersFromSeedValidated, setUsers, getUsersValidated,
};
