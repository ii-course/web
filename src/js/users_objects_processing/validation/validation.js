import libphonenumber from 'google-libphonenumber';
import { upperFirst } from 'lodash';
import { countryCodes } from '../data/country_codes';
import { isLetterInUpperCase } from '../../strings_processing';

const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();

function objectContainsField(object, field) {
  return Object.prototype.hasOwnProperty.call(object, field);
}

function getCountryCode(countryName) {
  const countryFound = countryCodes.find((country) => country.name === countryName);
  return countryFound.code;
}

function checkIfAgeValid(age) {
  if (typeof age !== 'number') {
    throw new Error(`Age has invalid type '${typeof age}'`);
  }
  if (age <= 0) {
    throw new Error(`Age is not positive: ${age}`);
  }
}

function checkIfPhoneValidForCountry(number, country) {
  const countryCode = getCountryCode(country);
  const parsedNumber = phoneUtil.parse(number, countryCode);
  if (!phoneUtil.isValidNumberForRegion(parsedNumber, countryCode)) {
    throw new Error(`Invalid phone format for ${country}: ${number}`);
  }
  // or (without lib):
  // const regex = new RegExp(/\+?\(?(\d+)\)?(\s?(\d+-?|-?\d+)\s?)+/);
  // if (!regex.test(number.toString())) {
  //   throw new Error(`Phone is in invalid format: ${number}`);
  // }
}

function checkIfEmailValid(email) {
  if (typeof email !== 'string') {
    throw new Error(`Email has invalid type '${typeof email}'`);
  }
  if (email.length <= 0) {
    throw new Error('Email length is less than or equal to 0');
  }
  const regex = new RegExp(/.+@.+\..+/);
  if (!regex.test(email)) {
    throw new Error(`Email is in invalid format: ${email}. Should be: [example]@[example].[com/net/etc]`);
  }
}

function checkIfStringFieldIsValid(fieldName, fieldValue) {
  if (fieldValue) {
    if (typeof fieldValue !== 'string') {
      throw new Error(`${fieldName} has invalid type '${typeof fieldValue}'`);
    }
    if (fieldValue.length <= 0) {
      throw new Error(`${fieldName} length is less than or equal to 0`);
    }
    if (!isLetterInUpperCase(fieldValue[0])) {
      throw new Error(`First letter in a ${fieldName} is in lower case or is not a letter: ${fieldValue}`);
    }
  }
}

function validateStringFields(user, fields) {
  fields.forEach((field) => {
    if (objectContainsField(user, field)) {
      checkIfStringFieldIsValid(field, user[field]);
    }
  });
}

function validateFieldsByFunctionsProvided(user, fieldsWithValidationFunctions) {
  Object.keys(fieldsWithValidationFunctions).forEach((field) => {
    if (objectContainsField(user, field)) {
      const validateField = fieldsWithValidationFunctions[field];
      validateField(user[field]);
    }
  });
}

function validatePhoneField(user) {
  if (objectContainsField(user, 'phone') && user.phone) {
    checkIfPhoneValidForCountry(user.phone, user.country);
  }
}

const stringFields = ['full_name', 'gender', 'country', 'state', 'city', 'note'];
const numericFields = ['age'];

function fixStringFields(user) {
  stringFields.forEach((field) => {
    if (user[field]) {
      // eslint-disable-next-line no-param-reassign
      user[field] = upperFirst(user[field]);
    }
  });
}

function fixNumericFields(user) {
  numericFields.forEach((field) => {
    if (typeof field !== 'number') {
      try {
        // eslint-disable-next-line no-param-reassign
        user[field] = Number.parseInt(user[field], 10);
      } catch (e) {
        console.log(`Cannot convert to number: ${field}: ${user[field]}`);
      }
    }
  });
}

function fixDataFormatWherePossible(user) {
  fixStringFields(user);
  fixNumericFields(user);
}

export function validateUser(user) {
  fixDataFormatWherePossible(user);
  const miscFieldsWithValidationMethods = {
    age: checkIfAgeValid,
    email: checkIfEmailValid,
  };
  try {
    validateStringFields(user, stringFields);
    validateFieldsByFunctionsProvided(user, miscFieldsWithValidationMethods);
    validatePhoneField(user);
  } catch (error) {
    // console.log(error);
    return false;
  }
  return true;
}

export function validateUsers(usersToValidate) {
  usersToValidate.forEach((user) => {
    validateUser(user);
  });
}
