import { rewriteUsers as rewriteUsersForUi } from './users_for_ui_with_changes_tracked';
import { rewriteUsers as rewriteUsersForStatistics } from '../html_page_scripts/current_users_for_statistics/users';
import { updateTopTeachers } from '../html_page_scripts/top_teachers_gallery/filters';

export default function updateUsersEverywhere() {
  rewriteUsersForUi();
  rewriteUsersForStatistics();
  updateTopTeachers();
}
