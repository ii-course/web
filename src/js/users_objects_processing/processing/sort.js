import { sortBy, reverse } from 'lodash';

export const sortOrder = {
  ASC: 0, // small -> big, a -> z
  DESC: 1, // big -> small, z -> a
};

export function sortUsersBy(users, field, sortOrderParam = sortOrder.ASC) {
  const sorted = sortBy(users, (user) => user[field]);
  switch (sortOrderParam) {
    case sortOrder.ASC:
      return sorted;
    case sortOrder.DESC:
      return reverse(sorted);
    default:
      throw new Error('Invalid sort order provided');
  }
}
