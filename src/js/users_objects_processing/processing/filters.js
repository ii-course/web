import getPercentageFromArraysLength from './maths';

export const operationSymbols = {
  EQUAL: '===',
  LESS: '<',
  MORE: '>',
  LESS_OR_EQUAL: '<=',
  MORE_OR_EQUAL: '>=',
};

export function filterByAge(users, age, operation = operationSymbols.EQUAL) {
  const usersWithAgeMentioned = users.filter((user) => !!user.age);
  const comparer = {
    '===': (user) => user.age === age,
    '<': (user) => user.age < age,
    '>': (user) => user.age > age,
    '<=': (user) => user.age <= age,
    '>=': (user) => user.age >= age,
  };
  return usersWithAgeMentioned.filter(comparer[operation]);
}

export function filterByStringField(users, field, value) {
  return users.filter((user) => user[field]?.toLowerCase() === value.toLowerCase());
}

export function filterByFavourites(users, isFavourite = true) {
  return users.filter((user) => (user.favourite === isFavourite)
    || (typeof user.favourite === 'undefined' && isFavourite === false));
}

export function getPercentageOfUsersFilteredByAge(users, age, operation = operationSymbols.EQUAL) {
  let usersFiltered = 0;
  try {
    usersFiltered = filterByAge(users, age, operation);
  } catch (error) {
    console.error(error);
    return 0;
  }
  return getPercentageFromArraysLength(usersFiltered, users);
}

export function getPercentageOfUsersFilteredByStringField(users, field, value) {
  const usersFiltered = filterByStringField(users, field, value);
  return getPercentageFromArraysLength(usersFiltered, users);
}

export function getPercentageOfUsersFilteredByFavourites(users, isFavourite = true) {
  const usersFiltered = filterByFavourites(users, isFavourite);
  return getPercentageFromArraysLength(usersFiltered, users);
}
