import getPercentageFromArraysLength from './maths';

const stringFieldsWithStrictComparison = ['gender'];

function findAllByStringValue(users, fieldName, valueToFind) {
  return users.filter((user) => user[fieldName]?.toLowerCase().includes(valueToFind.toLowerCase()));
}

function findAllByStringValueStrictly(users, fieldName, valueToFind) {
  return users.filter((user) => user[fieldName]?.toLowerCase() === valueToFind.toLowerCase());
}

function findAllByNumberValue(users, fieldName, valueToFind) {
  return users.filter((user) => user[fieldName] === valueToFind);
}

function findFirstByStringValue(users, fieldName, valueToFind) {
  return users.find((user) => user[fieldName]?.toLowerCase().includes(valueToFind.toLowerCase()));
}

function findFirstByStringValueStrictly(users, fieldName, valueToFind) {
  return users.find((user) => user[fieldName]?.toLowerCase() === valueToFind.toLowerCase());
}

function findFirstByNumberValue(users, fieldName, valueToFind) {
  return users.find((user) => user[fieldName] === valueToFind);
}

export function findById(users, id) {
  return users.find((user) => user.id === id);
}

const finder = {
  forStringField: findAllByStringValue,
  forNumberField: findAllByNumberValue,
};

function findByFinderFunctions(users, fieldName, valueToFind) {
  const typeOfValueToFind = typeof valueToFind;
  if (typeOfValueToFind === 'string') {
    return finder.forStringField(users, fieldName, valueToFind);
  }
  if (typeOfValueToFind === 'number') {
    return finder.forNumberField(users, fieldName, valueToFind);
  }
  throw new Error(`Unsupported type for search: ${typeOfValueToFind}`);
}

export function findFirstBy(users, fieldName, valueToFind) {
  if (stringFieldsWithStrictComparison.includes(fieldName)) {
    finder.forStringField = findFirstByStringValueStrictly;
  } else {
    finder.forStringField = findFirstByStringValue;
  }
  finder.forNumberField = findFirstByNumberValue;
  return findByFinderFunctions(users, fieldName, valueToFind);
}

export function findAllBy(users, fieldName, valueToFind) {
  if (stringFieldsWithStrictComparison.includes(fieldName)) {
    finder.forStringField = findAllByStringValueStrictly;
  } else {
    finder.forStringField = findAllByStringValue;
  }
  finder.forNumberField = findAllByNumberValue;
  return findByFinderFunctions(users, fieldName, valueToFind);
}

export function getPercentageOfUsersFound(users, fieldName, valueToFind) {
  let usersFound = 0;
  try {
    usersFound = findAllBy(users, fieldName, valueToFind);
  } catch (error) {
    console.error(error);
    return 0;
  }
  return getPercentageFromArraysLength(usersFound, users);
}
