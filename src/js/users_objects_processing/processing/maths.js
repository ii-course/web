function getPercentage(part, total) {
  return Math.round((part / total) * 100);
}

export default function getPercentageFromArraysLength(innerArray, outerArray) {
  return getPercentage(innerArray.length, outerArray.length);
}
