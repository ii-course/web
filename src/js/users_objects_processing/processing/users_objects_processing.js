import {
  filterByAge,
  filterByFavourites,
  filterByStringField,
  operationSymbols,
} from './filters';
import { sortUsersBy } from './sort';
import { findAllBy } from './search';
import { users } from '../users_for_ui_with_changes_tracked';

export function getFilteredUsers(options) {
  let usersCopy = [...users];
  if (options.country.length > 0) {
    usersCopy = filterByStringField(usersCopy, 'country', options.country);
  }
  if (options.age.length > 0) {
    usersCopy = filterByAge(
      usersCopy, Number.parseInt(options.age, 10), operationSymbols[options.ageOperation],
    );
  }
  if (options.gender.length > 0) {
    usersCopy = filterByStringField(usersCopy, 'gender', options.gender);
  }
  if (options.isFavourite.length > 0) {
    usersCopy = filterByFavourites(usersCopy, options.isFavourite === 'true');
  }
  return usersCopy;
}

export function getSortedUsers(options) {
  const usersCopy = [...users];
  sortUsersBy(usersCopy, options.fieldToSort, options.order);
  return usersCopy;
}

export function getUsersFound(options) {
  let usersCopy = [...users];
  if (options.name.length > 0) {
    usersCopy = findAllBy(usersCopy, 'full_name', options.name);
  }
  if (options.note.length > 0) {
    usersCopy = findAllBy(usersCopy, 'note', options.note);
  }
  if (options.ageString.length > 0) {
    const age = Number.parseInt(options.ageString, 10);
    usersCopy = findAllBy(usersCopy, 'age', age);
  }
  return usersCopy;
}

export function getTopTeachers() {
  return users.slice(0, 10);
}

export function getFavouriteTeachers() {
  return users.filter((user) => user.favorite === true);
}
