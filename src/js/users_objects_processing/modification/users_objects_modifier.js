import { pullAt } from 'lodash';
import { additionalUsers, randomUserMock } from '../data/mock_for_L3';

export const idGenerator = {
  currentId: 0,
  getNextId: () => {
    const idToReturn = idGenerator.currentId;
    idGenerator.currentId += 1;
    return idToReturn.toString();
  },
};

function getUserObjectInNewFormat(userObjectOld) {
  return {
    gender: userObjectOld.gender,
    title: userObjectOld.name.title,
    full_name: `${userObjectOld.name.first} ${userObjectOld.name.last}`,
    city: userObjectOld.location.city,
    state: userObjectOld.location.state,
    country: userObjectOld.location.country,
    postcode: userObjectOld.location.postcode,
    coordinates: {
      latitude: Number.parseFloat(userObjectOld.location.coordinates.latitude),
      longitude: Number.parseFloat(userObjectOld.location.coordinates.longitude),
    },
    timezone: userObjectOld.location.timezone,
    email: userObjectOld.email,
    b_day: userObjectOld.dob.date,
    age: userObjectOld.dob.age,
    phone: userObjectOld.phone,
    picture_large: userObjectOld.picture.large,
    picture_thumbnail: userObjectOld.picture.thumbnail,
    id: userObjectOld.id.value == null ? idGenerator.getNextId() : userObjectOld.id.value,
  };
}

export function changeAllUsersObjectsToNewFormat(users) {
  return users.map(getUserObjectInNewFormat);
}

export function merge(mainUserArray, additionalUserArray) {
  const additionalArrayFiltered = [...additionalUserArray]; // clone
  const mainArrayMapped = mainUserArray.map((user) => {
    const indexInAdditionalArray = additionalArrayFiltered.findIndex(
      (userAdditional) => userAdditional.full_name === user.full_name,
    );
    if (indexInAdditionalArray >= 0) {
      const additionalUserInfo = additionalArrayFiltered[indexInAdditionalArray];
      pullAt(additionalArrayFiltered, indexInAdditionalArray);
      return Object.assign(user, additionalUserInfo);
    }
    return user;
  });
  return mainArrayMapped.concat(additionalArrayFiltered);
}

export function getFormattedAndMergedUsersArray() {
  const formattedUsersArray = changeAllUsersObjectsToNewFormat(randomUserMock);
  return merge(formattedUsersArray, additionalUsers);
}
