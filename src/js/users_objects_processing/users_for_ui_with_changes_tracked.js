import { users as usersValidated } from './validation/users_validated';
import { validateUser } from './validation/validation';
import { idGenerator } from './modification/users_objects_modifier';

// eslint-disable-next-line import/no-mutable-exports
export let users = [];

export function rewriteUsers() {
  users = usersValidated;
}

export function markUserByIdAsFavourite(isFavourite, id) {
  const i = users.findIndex((user) => user.id === id);
  users[i].favorite = isFavourite;
}

export function replaceCurrentUsersWith(newUsers) {
  users = [...newUsers];
}

export function addUser(user) {
  if (validateUser(user)) {
    // eslint-disable-next-line no-param-reassign
    user.id = idGenerator.getNextId();
    users.push(user);
    return true;
  }
  return false;
}
