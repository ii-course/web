import { sortOrder, sortUsersBy } from '../processing/sort';

export function showResultOfSortingByField(users, field, sortOrderParam = sortOrder.ASC) {
  let i = 1;
  sortUsersBy(users, field, sortOrderParam).forEach((user) => {
    console.log(`[${i}][Sort: ${field} ${sortOrderParam === sortOrder.ASC ? 'ASC' : 'DESC'}] `
      + `User: ${user.full_name}${field === 'full_name' ? '' : `, ${field}: ${user[field]}`}`);
    i += 1;
  });
}

export function showExampleOfSortingByDifferentFields(users) {
  const fieldsToTest = ['age', 'b_date', 'country', 'full_name'];
  fieldsToTest.forEach((field) => {
    console.log(`\nSort by ${field.toUpperCase()}`);
    showResultOfSortingByField(users, field);
    showResultOfSortingByField(users, field, sortOrder.DESC);
  });
}
