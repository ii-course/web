import {
  operationSymbols, filterByAge, filterByStringField, filterByFavourites,
  getPercentageOfUsersFilteredByAge, getPercentageOfUsersFilteredByStringField,
  getPercentageOfUsersFilteredByFavourites,
} from '../processing/filters';

export function showResultOfFilteringByAge(users, age, operation = operationSymbols.EQUAL) {
  filterByAge(users, age, operation).forEach((user) => console.log(
    `[Filter: age ${operation} ${age}] User: ${user.full_name}, age: ${user.age}`,
  ));
}

export function showResultOfFilteringByCountry(users, country) {
  filterByStringField(users, 'country', country).forEach((user) => console.log(
    `[Filter: country is ${country}] User: ${user.full_name}, country: ${user.country}`,
  ));
}

export function showResultOfFilteringByFavourites(users, isFavourite = true) {
  filterByFavourites(users, isFavourite).forEach((user) => console.log(
    `[Filter: ${isFavourite ? 'favourite' : 'not favourite'} users]`
    + `User: ${user.full_name}, is favourite: ${user.favourite}`,
  ));
}

export function showResultOfFilteringByGender(users, gender) {
  filterByStringField(users, 'gender', gender).forEach((user) => console.log(
    `[Filter: gender is ${gender}] User: ${user.full_name}, gender: ${user.gender}`,
  ));
}

export function showExamplesOfFilters(users) {
  showResultOfFilteringByAge(users, 30, operationSymbols.MORE_OR_EQUAL);
  showResultOfFilteringByCountry(users, 'United States');
  showResultOfFilteringByGender(users, 'Male');
}

export function showPercentageOfUsersFilteredByAge(users, age,
  operation = operationSymbols.EQUAL) {
  const percentage = getPercentageOfUsersFilteredByAge(users, age, operation);
  console.log(`${percentage}% of users are ${operation} ${age} years old`);
}

export function showPercentageOfUsersFilteredByCountry(users, country) {
  const percentage = getPercentageOfUsersFilteredByStringField(users, 'country', country);
  console.log(`${percentage}% of users are from ${country}`);
}

export function showPercentageOfUsersFilteredByGender(users, gender) {
  const percentage = getPercentageOfUsersFilteredByStringField(users, 'gender', gender);
  console.log(`${percentage}% of users are ${gender}`);
}

export function showPercentageOfUsersFilteredByFavourites(users, isFavourite = true) {
  const percentage = getPercentageOfUsersFilteredByFavourites(users, isFavourite);
  console.log(`${percentage}% of users ${isFavourite ? 'are' : 'are not'} favourite`);
}

export function showExamplesOfGettingPercentage(users) {
  showPercentageOfUsersFilteredByAge(users, 30, operationSymbols.MORE);
  showPercentageOfUsersFilteredByCountry(users, 'United States');
  showPercentageOfUsersFilteredByGender(users, 'Female');
}
