import { findAllBy, getPercentageOfUsersFound } from '../processing/search';

export function showSearchResults(users, fieldName, fieldValue) {
  const usersFound = findAllBy(users, fieldName, fieldValue);
  let i = 1;
  usersFound.forEach((user) => {
    console.log(`[${i}][Search by ${fieldName}: ${fieldValue}] User: ${user.full_name}`
    + `${fieldName === 'full_name' ? '' : `, ${fieldName}: ${user[fieldName]}`}`);
    i += 1;
  });
}

export function showPercentageOfUsersFound(users, fieldName, fieldValue) {
  const percentage = getPercentageOfUsersFound(users, fieldName, fieldValue);
  console.log(`${percentage}% of users have ${fieldName} ${typeof fieldValue === 'string' ? `'${fieldValue}'` : fieldValue}`);
}

export function showExamplesOfSearch(users) {
  const fieldsToSearch = { name: 'Sara', note: 'cats', age: 30 };
  Object.keys(fieldsToSearch).forEach((fieldName) => {
    const fieldValue = fieldsToSearch[fieldName];
    showSearchResults(users, fieldName, fieldValue);
    showPercentageOfUsersFound(users, fieldName, fieldValue);
  });
}
