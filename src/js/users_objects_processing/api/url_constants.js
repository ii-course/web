export const RAW_URL = new URL('https://randomuser.me/api');
export const SEED = 'foobar';
export const SERVER_URL = new URL('http://localhost:3000/users');
