import { RAW_URL } from './url_constants';

export default function getUsersRawData(params) {
  const urlToFetch = Object.assign(RAW_URL);
  if (params) {
    urlToFetch.search = new URLSearchParams(params).toString();
  }
  return fetch(urlToFetch.toString())
    .then((response) => response?.json())
    .catch((error) => {
      console.error(error);
    });
}
