import { goToPage } from './html_page_scripts/statistics_table/pagination/pagination';
import showTopTeachers from './html_page_scripts/top_teachers_gallery/display';
import showFavouriteTeachers from './html_page_scripts/line_gallery/display';
import addListeners from './html_page_scripts/listeners';
import { setUsers } from './users_objects_processing/validation/users_validated';
import updateUsersEverywhere from './users_objects_processing/users_update';

// json-server --watch ./src/js/users.json

const MAX_NUM_OF_REQUESTS = 10;
let currentNumOfRequests = 0;

function setUsersFromFetchEverywere() {
  setUsers()
    .then(() => {
      updateUsersEverywhere();
      showTopTeachers();
      showFavouriteTeachers();
      goToPage(1);
    })
    .catch((error) => {
      console.error(error);
      if (currentNumOfRequests < MAX_NUM_OF_REQUESTS) {
        currentNumOfRequests += 1;
        setUsersFromFetchEverywere();
      } else {
        console.log('Reached the limit of requests');
      }
    });
}

window.addEventListener('load', () => {
  addListeners();
  setUsersFromFetchEverywere();
});
